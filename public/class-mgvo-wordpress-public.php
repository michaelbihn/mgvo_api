<?php

/**
 * The public-facing functionality of the plugin.
 *

 * @since      1.0.0
 *
 * @package    Mgvo_Wordpress
 * @subpackage Mgvo_Wordpress/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Mgvo_Wordpress
 * @subpackage Mgvo_Wordpress/public
 * @author     Michael Kours <webmaster@blau-gold-darmstadt.de>
 */
class Mgvo_Wordpress_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	private $func;
	
	public $call_id;
	
	public $cache;
	
	private $mgvo_secret;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$options = get_option( $this->plugin_name . '-options' );
		//require_once(plugin_dir_path( dirname( __FILE__ ) ) . "includes/class-mgvo-wordpress-functions.php");
		require_once(plugin_dir_path( dirname( __FILE__ ) ) . "mgvo-snipplets/mgvo_sniplets.php");
		$this->call_id = $options['mgvo_call_id'];
		$this->mgvo_secret = $options['mgvo_secret'];
		$this->cache = $options['mgvo_chache_time'];
		$this->func = new MGVO_SNIPLET($this->call_id,$this->mgvo_secret, $this->cache);
		//$this->func =  new MGVO_Wordpress_Functions($options); 
		
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Mgvo_Wordpress_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Mgvo_Wordpress_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/mgvo-wordpress-public.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Mgvo_Wordpress_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Mgvo_Wordpress_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/mgvo-wordpress-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * shortcut: [groups]
	 */
	public function mgvo_shortcode_gruppen($atts = []) {
		// outputs a list of groups
		$atts = array_change_key_case((array)$atts, CASE_LOWER);
		$a = shortcode_atts( array(
			'category' => ''
		), $atts );	
        if ($a['category']=="") { // Beispielcode für Parameter aus der URL
			if (isset($_GET['category'])) {
				$a['category']=$_GET['category'];
			}
		} 
		return $this->func->mgvo_sniplet_gruppen($a); 	

	}

	
}
