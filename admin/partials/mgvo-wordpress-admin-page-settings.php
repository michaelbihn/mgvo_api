<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Mgvo_Wordpress
 * @subpackage Mgvo_Wordpress/admin/partials
 */
?>

<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
<form method="post" action="options.php"><?php

settings_fields( $this->plugin_name . '-options' );

do_settings_sections( $this->plugin_name );

phpinfo();

?>

Platzhalter für den Admin Bereich aus mgvo-wordpress-admin-page-settings.php

<?php


submit_button( 'Save Settings' );

?></form>
