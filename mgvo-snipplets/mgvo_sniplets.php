<?php

   require_once(plugin_dir_path(dirname( __FILE__ ))."mgvo-api/ext_mod_hp.php");
   
   class MGVO_SNIPLET {
      private $api;
            
      function __construct($call_id,$vcryptkey, $cachemin) {
         // call_id: call_id des Vereins
         // vcryptkey: Schlüssel für die synchrone Verschlüsselung. Wird in MGVO in den technischen Parametern eingetragen 
         // $vcryptkey = $vp[0];
         // $cachemin = isset($vp[1]) ? $vp[1] : 5; 
         $this->api = new MGVO_HPAPI($call_id, $vcryptkey, $cachemin);
      }
       
      
      function mgvo_sniplet_vkal($vkalnr,$seljahr) {
         // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr
		 $vkal = $this->api->read_vkal($vkalnr,$seljahr);
		 
		 //  Hier sollte der Code stehen, der aus den Ergebnis eine HTML-Struktur macht
		 $sniplet = "<div> Test Kalenderausgabe</div>";
 
         return $sniplet;
      }
      
      function mgvo_sniplet_orte() {
         // Liest die Ortsliete ein
		 $orte = $this->api->read_orte();
		 
		 //  Hier sollte der Code stehen, der aus den Ergebnis eine HTML-Struktur macht
		 $sniplet = "<div> Test Otrsausgabe</div>";
 
         return $sniplet;
      }
            
      function mgvo_sniplet_betreuer() {
         // Liest die Betreuer ein
		 $betreuer = $this->api->read_betreuer();
		 
		 //  Hier sollte der Code stehen, der aus den Ergebnis eine HTML-Struktur macht
		 $sniplet = "<div> Test Betreuerausgabe</div>";
 
         return $sniplet;
      }
      
      function mgvo_sniplet_events() {
         // Liest die öffentlichen Veranstaltungen
		 $events = $this->api->read_evants();
		 
		 //  Hier sollte der Code stehen, der aus den Ergebnis eine HTML-Struktur macht
		 $sniplet = "<div> Test Veranstaltungsausgabe</div>";
 
         return $sniplet;
      }
      
      function mgvo_sniplet_gruppen($a) { 
 		 $gruppen = $this->api->read_gruppen();
		 
		 //  Hier sollte der Code stehen, der aus den Ergebnis eine HTML-Struktur macht, die Paraemter aus $a sollten weitergegeben oder berücksichtigt werden
		 $sniplet = "<div> Test Gruppenausgabe</div>";
 
         return $sniplet;
      }
      
      function mgvo_sniplet_abteilungen() {
	     $abt = $this->api->read_abt();
		 
		 //  Hier sollte der Code stehen, der aus den Ergebnis eine HTML-Struktur macht
		 $sniplet = "<div> Test VAbteilungsausgabe</div>";
 
         return $sniplet;
      }
      
      function mgvo_sniplet_training_fail() {
	     $training_fail = $this->api->read_training_fail();
		 
		 //  Hier sollte der Code stehen, der aus den Ergebnis eine HTML-Struktur macht
		 $sniplet = "<div class='mgvo mgvo-sniplet mgvo-gruppen'> Test Veranstaltungsausgabe</div>";
 
         return $sniplet;
      }
      
      function mgvo_sniplet_sel_mitglieder($selparar) {
         // Selektion von Mitgliedern. 
  	     $mitgleider = $this->api->sel_mitglieder($selparar);
		 
		 //  Hier sollte der Code stehen, der aus den Ergebnis eine HTML-Struktur macht
		 $sniplet = "<div> Test Veranstaltungsausgabe</div>";
 
         return $sniplet;
      }
      
      function mgvo_sniplet_show_mitglied($mgnr) {
	     $mitglied = $this->api->show_mitglied($mgnr);
		 
		 //  Hier sollte der Code stehen, der aus den Ergebnis eine HTML-Struktur macht
		 $sniplet = "<div> Test Veranstaltungsausgabe</div>";
 
         return $sniplet;
      }
      
      /*function list_documents(...$vp) {
         // dokart: Es werden nur Dokumente dieser Dokumentart aufgelistet
         $dokart = $vp[0];
         $this->cacheon = 1;
         $vars['call_id'] = $this->call_id;
         $vars['dokart'] = $dokart;
         $paras = http_build_query($vars);
         $url = "$this->urlroot/pub_documents_xml.php?$paras";
         $this->tab = $this->xml2table($url,$paras);
         $ergar = $this->create_ergar("documentlist","document","mgar");
         return $ergar;
      } */
      

      
   }
   
?>